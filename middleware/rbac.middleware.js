const APP_ROLES = {
  admin: 'ELVAdmin',
  editor: 'ELVEditor',
  user: 'ELVUser',
  guest: 'ELVGuest'
};

// SETTING ACCESS LEVEL
function rbac(req, res, next) {
  switch (true) {
    case req.user.hasOwnProperty('roles') === false:
      req.accessLevel = APP_ROLES.editor;
      return next();
    case req.user.roles.some(role => role === APP_ROLES.admin):
      req.accessLevel = APP_ROLES.admin;
      return next();
    case req.user.roles.some(role => role === APP_ROLES.editor):
      req.accessLevel = APP_ROLES.editor;
      return next();
    default:
      return res
        .status(403)
        .send('No valid user permissions found, this should not happen.');
  }
}

module.exports = rbac;
