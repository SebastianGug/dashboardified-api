const errorMiddleware = (error, req, res, next) => {
  switch (error.name) {
    case 'database':
      console.log('Error handler middleware DATABASE firing: ', error);
      return res.status(500).send('Unknown database error.');
    case 'invalidInput':
      console.log('Error handler middleware INVALID INPUT firing: ', error);
      return res.status(400).send(error.details[0].message);
    case 'permissions':
      console.log('Error handler middleware PERMISSIONS firing');
      return res.status(403).send('You do not have permission to do this');
    case 'not-a-tenant':
      console.log('Error handler middleware NOT A TENANT firing');
      return res.send({
        isTenant: false,
        message: 'You do not currently have a valid Dashboardified license.'
      });
    case 'no-matches':
      console.log('Error handler middleware NOT YOUR DATA firing');
      return res.status(404).send('Collection not found or no longer exists');
    default:
      console.log('DEFAULT ERROR MIDDLEWARE FIRING: ', error);
      res.status(500).send('Unknown Internal Server Error');
  }
};

module.exports = errorMiddleware;
