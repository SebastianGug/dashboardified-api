function tenantChecker(req, res, next) {
  req.loggedTenant = global.tenantCatalog.find(
    tenant => tenant.tid === req.user.tid
  );

  if (req.loggedTenant && req.loggedTenant.tid === req.user.tid) {
    next();
  } else {
    err = new Error();
    err.name = 'not-a-tenant';
    next(err);
  }
}

module.exports = tenantChecker;
