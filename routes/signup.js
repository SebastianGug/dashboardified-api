const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validate = require('../validators/signup-validation');
const appconfig = require('config');
// const Recaptcha = require('express-recaptcha').Recaptcha;
// const recaptcha = new Recaptcha(
//   appconfig.get('config.captchaSite'),
//   appconfig.get('config.captchaSecret')
// );

// KNEX configuration
let knex = require('knex')({
  client: 'mssql',
  connection: {
    host: appconfig.get('dashboardified.address'),
    user: appconfig.get('dashboardified.username'),
    password: appconfig.get('dashboardified.password'),
    database: 'dashboardified',
    encrypt: true
  }
});

// router.use(function(req, res, next) {
//   // TODO: Implement recaptcha validation here on the backend
//   next();
// });

router.post('/subscribe', (req, res) => {
  const validity = validate.newSignup(
    req.body.email,
    req.body.firstName,
    req.body.lastName,
    req.body.requestedAccess
  );

  if (validity.error) {
    res.status(400).send(validity.error.details[0].message);
  } else {
    knex('subscribers')
      .where({ email: req.body.email })
      .select('email')
      .catch(err => {
        console.log(err.message);
        res
          .status(500)
          .send('Database error when checking if subscriber exists.');
      })
      .then(data => {
        if (data[0]) {
          res
            .status(400)
            .send(data[0].email + ' already exists in the database.');
        } else {
          let requested_access;
          if (req.body.requestedAccess === true) {
            requested_access = 1;
          } else if (req.body.requestedAccess === false) {
            requested_access = 0;
          } else {
            res
              .status(500)
              .send('THIS SHOULDNT HAPPEN, JOI VALIDATION FAILED MISERABLY');
          }
          knex('subscribers')
            .insert({
              email: req.body.email,
              first_name: req.body.firstName,
              last_name: req.body.lastName,
              requested_access: requested_access,
              joined: knex.raw('SYSDATETIME()')
            })
            .catch(err => {
              console.log(err.message);
              res.status(500).send('Database error when trying subscribe.');
            })
            .then(data => {
              let success = req.body.email + ' successfully registered.';
              res.send(success);
            });
        }
      });
  }
});

module.exports = router;
