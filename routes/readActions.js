const express = require('express');
const router = express.Router();
const crypto = require('../utils/encryption');
const appconfig = require('config');
const validate = require('../validators/dashboardified-validation');

let knex = require('knex')({
  client: 'mssql',
  connection: {
    host: appconfig.get('dashboardified.address'),
    user: appconfig.get('dashboardified.username'),
    password: appconfig.get('dashboardified.password'),
    database: 'dashboardified',
    encrypt: true
  }
});

router.get('/is-tenant', (req, res) => {
  res.send({ isTenant: true });
});

// INITIALISING DATA TO BE DONE
router.get('/initialising', async (req, res, next) => {
  knex
    .select(
      'tenants.name',
      'tenants.description',
      'billing_status.name AS billing_status',
      'operational_status.name AS operational_status'
    )
    .from('tenants')
    .where({ 'tenants.tid': req.loggedTenant.tid })
    .leftJoin('billing_status', 'tenants.billing_key', 'billing_status.id')
    .leftJoin(
      'operational_status',
      'tenants.operational_key',
      'operational_status.id'
    )
    .catch(err => {
      console.log(err.message);
      res.status(500).send('Database error when getting initialising data.');
    })
    .then(data => {
      res.send(data);
    });
});

// DEFAULT

router.get('/dashboards/:id', async (req, res, next) => {
  try {
    let dashboards = await getAllDashboards();
    if (dashboards.length === 0) {
      res.send(dashboards);
    } else {
      let columns = await getColumns(dashboards[0].id);
      let graphColumns = await makeGraphArray(columns);

      const result = {
        dashboards: dashboards,
        columns: columns,
        graphColumns: graphColumns
      };
      res.send(result);
    }
  } catch (err) {
    err.name = 'database';
    next(err);
  }

  function getAllDashboards() {
    return knex
      .select(
        'dashboards.id',
        'dashboards.name',
        'dashboards.description',
        'dashboards.site',
        'dashboards.list',
        'dashboards.collection AS collectionID',
        'collections.name AS collection'
      )
      .from('dashboards')
      .leftJoin('collections', 'dashboards.collection', '=', 'collections.id')
      .where({
        'dashboards.tid': req.user.tid,
        'dashboards.collection': req.params.id
      });
  }

  function getColumns(dashboard) {
    return knex
      .select('columns.name', 'columns.displayName')
      .from('columns')
      .where({
        'columns.tid': req.user.tid,
        'columns.dashboard': dashboard
      });
  }

  function makeGraphArray(columns) {
    graphColumns = [];
    for (let column of columns) {
      this.graphColumns.push(column.name);
    }
    return graphColumns;
  }
});

router.get('/dashboard/:id', async (req, res, next) => {
  validateID();

  try {
    let dashboard = await getDashboard();
    let columns = await getColumns(req.params.id);
    let graphColumns = await makeGraphArray(columns);

    const result = {
      dashboard: dashboard,
      columns: columns,
      graphColumns: graphColumns
    };
    res.send(result);
  } catch (err) {
    err.name = 'database';
    next(err);
  }

  function getDashboard() {
    return knex
      .select(
        'dashboards.id',
        'dashboards.name',
        'dashboards.description',
        'dashboards.site',
        'dashboards.list',
        'dashboards.collection AS collectionID',
        'collections.name AS collection'
      )
      .from('dashboards')
      .leftJoin('collections', 'dashboards.collection', '=', 'collections.id')
      .where({
        'dashboards.tid': req.user.tid,
        'dashboards.id': req.params.id
      });
  }

  function getColumns(dashboard) {
    return knex
      .select('columns.name', 'columns.displayName')
      .from('columns')
      .where({
        'columns.tid': req.user.tid,
        'columns.dashboard': dashboard
      });
  }

  function makeGraphArray(columns) {
    graphColumns = [];
    for (let column of columns) {
      this.graphColumns.push(column.name);
    }
    return graphColumns;
  }

  function validateID() {
    const validity = validate.databaseID(req.params.id);
    if (validity.error) {
      validity.error.name = 'invalidInput';
      next(validity.error);
    } else {
      return;
    }
  }
});

// READ
// COLLECTIONS
router.get('/all-collections', async (req, res, next) => {
  try {
    collections = await getCollections();
    res.send(collections);
  } catch (err) {
    err.name = 'database';
    next(err);
  }

  function getCollections() {
    return knex
      .select(
        'collections.id',
        'collections.name',
        'collections.description',
        'collections.position',
        'tenants.default_collection'
      )
      .from('collections')
      .where({ 'collections.tid': req.user.tid })
      .fullOuterJoin('tenants', 'collections.id', 'tenants.default_collection');
  }
});

router.get('/collection/:name', async (req, res, next) => {
  validateName();

  try {
    collection = await getCollection();
    res.send(collection);
  } catch (err) {
    err.name = 'database';
    next(err);
  }

  function getCollection() {
    return knex
      .select(
        'collection.id',
        'collections.name',
        'collections.description',
        'collection.position'
      )
      .from('collections')
      .where({ tid: req.user.tid, name: req.params.name });
  }

  function validateName() {
    const validity = validate.name(req.params.name);
    if (validity.error) {
      validity.error.name = 'invalidInput';
      next(validity.error);
    } else {
      return;
    }
  }
});

router.get('/collection-full/:name', async (req, res, next) => {
  validateName();

  try {
    let collection = await getCollection();
    collection.dashboards = await getDashboards(collection);

    for (let dashboard of collection.dashboards) {
      knex
        .select('id', 'name', 'graph_name', 'position')
        .from('columns')
        .where({ tid: req.user.tid, id: dashboard.id })
        .then(columns => {
          return (dashboard[i].columns = columns);
        });
    }

    res.send(collection);
  } catch (err) {
    err.name = 'database';
    next(err);
  }

  function getDashboards(collection) {
    return knex
      .select('id', 'name', 'description', 'position')
      .from('dashboards')
      .where({ tid: req.user.tid, id: collection.id });
  }

  function getCollection() {
    return knex
      .select('id', 'name', 'description', 'position')
      .from('collections')
      .where({ tid: req.user.tid, name: req.params.name });
  }

  function validateName() {
    const validity = validate.name(req.params.name);
    if (validity.error) {
      validity.error.name = 'invalidInput';
      next(validity.error);
    } else {
      return;
    }
  }
});

// READ
// DASHBOARDS

router.get('/all-dashboards', async (req, res, next) => {
  try {
    dashboards = await getDashboards();
    res.send(dashboards);
  } catch (err) {
    err.name = 'database';
    next(err);
  }

  function getDashboards() {
    return knex
      .select(
        'dashboards.id',
        'dashboards.name',
        'dashboards.description',
        'dashboards.position',
        'dashboards.site',
        'dashboards.list',
        'dashboards.collection AS collectionID',
        'collections.name AS collection'
      )
      .from('dashboards')
      .where({ 'dashboards.tid': req.user.tid })
      .leftJoin('collections', 'dashboards.collection', 'collections.id')
      .orderBy('collections.name', 'ASC');
  }
});

// router.get('/dashboard/:name:id', async (req, res, next) => {
//   validateName();
//   try {
//     dashboard = await getDashboard();
//     res.send(dashboard);
//   } catch (err) {
//     err.name = 'database';
//     next(err);
//   }

//   function getDashboard() {
//     return knex
//       .select(
//         'dashboard.id',
//         'dashboard.name',
//         'dashboard.description',
//         'dashboard.position'
//       )
//       .from('dashboards')
//       .where({ tid: req.user.tid, name: req.params.name });
//   }

//   function validateName() {
//     const validity = validate.name(req.params.name);
//     if (validity.error) {
//       validity.error.name = 'invalidInput';
//       next(validity.error);
//     } else {
//       return;
//     }
//   }
// });

router.get('/dashboard-full/:name', async (req, res, next) => {
  validateName();

  try {
    let dashboard = await getDashboard();
    await getColumns(dashboard);
    res.send(dashboard);
  } catch (err) {
    err.name = 'database';
    next(err);
  }

  function getColumns(dashboard) {
    return knex
      .select('id', 'displayName', 'name', 'position')
      .from('columns')
      .where({ tid: req.user.tid, id: dashboard.id })
      .orderBy('position', 'ASC');
  }

  function getDashboard() {
    return knex
      .select(
        'dashboard.id',
        'dashboard.name',
        'dashboard.description',
        'dashboard.position'
      )
      .from('dashboards')
      .where({ tid: req.user.tid, name: req.params.name });
  }

  function validateName() {
    const validity = validate.name(req.params.name);
    if (validity.error) {
      validity.error.name = 'invalidInput';
      next(validity.error);
    } else {
      return;
    }
  }
});

router.get('/columns/:id', async (req, res, next) => {
  validateID();

  try {
    let columns = await checkDataOwner();
    res.send(columns);
  } catch (err) {
    err.name = 'database';
    next(err);
  }

  async function checkDataOwner() {
    return knex('columns')
      .select('*')
      .where({ dashboard: req.params.id, tid: req.user.tid })
      .then(data => {
        console.log(data);
        if (data.length > 0) {
          return data;
        } else {
          err = new Error();
          err.name = 'no-matches';
          return next(err);
        }
      });
  }

  function validateID() {
    const validity = validate.databaseID(req.params.id);
    if (validity.error) {
      validity.error.name = 'invalidInput';
      next(validity.error);
    } else {
      return;
    }
  }
});

module.exports = router;
