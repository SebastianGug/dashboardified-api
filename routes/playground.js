const express = require('express');
const router = express.Router();
const crypto = require('../utils/encryption');
const appconfig = require('config');
const validate = require('../validators/dashboardified-validation');

let knex = require('knex')({
  client: 'mssql',
  connection: {
    host: appconfig.get('dashboardified.address'),
    user: appconfig.get('dashboardified.username'),
    password: appconfig.get('dashboardified.password'),
    database: 'dashboardified',
    encrypt: true
  }
});

const tid = '1331a7e6-3ca0-4a21-a07e-82a168e77cd0';

router.get('/:id', async (req, res) => {
  let result = knex('collections')
    .select('id')
    .where({ id: req.params.id, tid: tid })
    .toString();

  res.send(result);
  // let dashboard = {
  //   name: 'Test Dashboard',
  //   description: 'Test description blablah',
  //   collection: '7',
  //   tid: 'f83ce3ee-4805-4cf3-878c-4ad63406e3ed',
  //   columns: [
  //     { name: 'First column', position: 0, isDate: null, location: null },
  //     { name: 'Second column', position: 1, isDate: null, location: null },
  //     { name: 'Third column', position: 2, isDate: null, location: null },
  //     { name: 'Fourth column', position: 3, isDate: null, location: null }
  //   ]
  // };

  // try {
  //   let result = await insertEverything();
  //   res.send(result);
  // } catch (err) {
  //   console.log(err);
  //   next(err);
  // }

  // function insertEverything() {
  //   return knex('dashboards')
  //     .returning('id')
  //     .insert({
  //       name: dashboard.name,
  //       description: dashboard.description,
  //       collection: dashboard.collection,
  //       modified: knex.raw('SYSDATETIME()'),
  //       tid: dashboard.tid
  //     })
  //     .union(function(id) {
  //       knex('columns')
  //         .returning('id')
  //         .insert(dashboard.columns);
  //     })
  //     .toString();
  // }
});

router.delete('', (req, res) => {
  try {
    knex('columns')
      .where({ dashboard: 1 })
      .delete()
      .then(data => {
        res.send('data');
      });
  } catch (err) {
    console.log(err);
    next(err);
  }
});

// router.delete('/dashboards', (req, res) => {
//   try {
//     knex('dashboards')
//       .where({ dashboard: 1 })
//       .delete()
//       .then(data => {
//         res.send('data');
//       });
//   } catch (err) {
//     console.log(err);
//     next(err);
//   }
// });

router.get('/starts', (req, res, next) => {
  let result = req.path.startsWith('/starts');
  res.send(result);
});

module.exports = router;
