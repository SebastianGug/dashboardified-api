const express = require('express');
const router = express.Router();
const appconfig = require('config');
const validate = require('../validators/dashboardified-validation');
const rbac = require('../middleware/rbac.middleware');
const knex = require('knex')({
  client: 'mssql',
  connection: {
    host: appconfig.get('dashboardified.address'),
    user: appconfig.get('dashboardified.username'),
    password: appconfig.get('dashboardified.password'),
    database: 'dashboardified',
    encrypt: true
  }
});

router.use(rbac);

// ADD DASHBOARD
router.post('', async (req, res, next) => {
  try {
    //VALIDATING STUFF
    validateDashboard();
    validateColumns();

    // INSERTING STUFF
    let dashboard = await insertDashboard();
    let columns = await insertColumns(dashboard[0].id);

    // MODELLING THE OBJECT FROM WHAT WAS RECEIVED AND SENDING AS RESULT
    dashboard[0].columns = columns;
    res.send(dashboard);
  } catch (err) {
    // MAIN ERROR CATCHER
    err.name = 'database';
    next(err);
  }

  // FUNCTIONS BEING USED TO VALIDATE AND INSERT
  function validateDashboard() {
    const validity = validate.newDashboard(
      req.body.name,
      req.body.description,
      req.body.collection,
      req.body.site,
      req.body.list
    );
    if (validity.error) {
      validity.error.name = 'invalidInput';
      next(validity.error);
    } else {
      return;
    }
  }

  function insertColumns(dashboardID) {
    let columns = [];
    for (let [index, column] of req.body.columns.entries()) {
      columns.push({
        displayName: column.displayName,
        tid: req.user.tid,
        name: column.name,
        dashboard: dashboardID,
        position: index,
        date: column.isDate,
        location: column.isLocation,
        latitude: column.isLatitude,
        longitude: column.isLongitude,
        legend: column.isLegend
      });
    }

    return knex('columns')
      .returning('*')
      .insert(columns);
  }

  function insertDashboard() {
    return knex('dashboards')
      .returning('*')
      .insert({
        name: req.body.name,
        description: req.body.description,
        collection: req.body.collection,
        site: req.body.site,
        list: req.body.list,
        modified: knex.raw('SYSDATETIME()'),
        tid: req.user.tid,
        position: 51
      });
  }

  function validateColumns() {
    for (let column of req.body.columns) {
      const validity = validate.newColumn(column);

      if (validity.error) {
        validity.error.name = 'invalidInput';
        next(validity.error);
        break;
      } else {
        return;
      }
    }
  }
});

// UPDATE DASHBOARD
router.put('', async (req, res, next) => {
  // VALIDATION
  validateDashboard();
  validateID();

  try {
    await checkDataOwner();
    let success = await updateDashboard();
    res.send(success);
  } catch (err) {
    err.name = 'database';
    next(err);
  }

  function checkDataOwner() {
    knex('dashboards')
      .select('id')
      .where({ id: req.body.id, tid: req.user.tid })
      .then(data => {
        if (data.length > 0) {
          return;
        } else {
          err = new Error();
          err.name = 'not-your-data';
          return next(err);
        }
      });
  }

  function validateDashboard() {
    const validity = validate.updateDashboard(
      req.body.name,
      req.body.description,
      req.body.site,
      req.body.list
    );
    if (validity.error) {
      validity.error.name = 'invalidInput';
      next(validity.error);
    } else {
      return;
    }
  }

  function validateID() {
    const validity = validate.databaseID(req.body.id);
    if (validity.error) {
      validity.error.name = 'invalidInput';
      next(validity.error);
    } else {
      return;
    }
  }

  function updateDashboard() {
    return knex('dashboards')
      .returning('*')
      .update({
        name: req.body.name,
        description: req.body.description,
        modified: knex.raw('SYSDATETIME()')
      })
      .where({ tid: req.user.tid, id: req.body.id });
  }
});

router.put('/column', async (req, res, next) => {
  validateColumn();
  validateID();

  try {
    await checkDataOwner();
    let column = await updateColumn();
    res.send(column);
  } catch (err) {
    err.name = 'database';
    next(err);
  }

  function updateColumn() {
    return knex('dashboards')
      .returning('*')
      .update({
        name: req.body.column.name,
        date: column.isDate,
        location: column.isLocation,
        latitude: column.isLatitude,
        longitude: column.isLongitude,
        legend: column.isLegend
      })
      .where({ tid: req.user.tid, id: req.body.id });
  }

  function checkDataOwner() {
    knex('columns')
      .select('id')
      .where({ id: req.body.id, tid: req.user.tid })
      .then(data => {
        if (data.length > 0) {
          return;
        } else {
          err = new Error();
          err.name = 'not-your-data';
          return next(err);
        }
      });
  }

  function validateColumn() {
    const validity = validate.newColumn(req.body.column);
    if (validity.error) {
      validity.error.name = 'invalidInput';
      next(validity.error);
    } else {
      return;
    }
  }

  function validateID() {
    const validity = validate.databaseID(req.body.id);
    if (validity.error) {
      validity.error.name = 'invalidInput';
      next(validity.error);
    } else {
      return;
    }
  }
});

router.put('/columns/', async (req, res, next) => {
  validateColumns();
  validateID();
  try {
    await deleteExistingColumns();
    let columns = await insertColumns();
    res.send(columns);
  } catch (err) {
    err.name = 'database';
    next(err);
  }

  function deleteExistingColumns() {
    return knex('columns')
      .where({ id: req.body.dashboard, tid: req.user.tid })
      .delete();
  }

  function insertColumns() {
    let columns = [];
    for (let [index, column] of req.body.columns.entries()) {
      columns.push({
        displayName: column.displayName,
        tid: req.user.tid,
        name: column.name,
        dashboard: req.body.dashboard,
        position: index,
        date: column.isDate,
        location: column.isLocation,
        latitude: column.isLatitude,
        longitude: column.isLongitude,
        legend: column.isLegend
      });
    }

    return knex('columns')
      .returning('*')
      .insert(columns);
  }

  function validateColumns() {
    for (let column of req.body.columns) {
      const validity = validate.existingColumns(column);

      if (validity.error) {
        validity.error.name = 'invalidInput';
        next(validity.error);
        break;
      } else {
        return;
      }
    }
  }
  function validateID() {
    const validity = validate.databaseID(req.body.dashboard);
    if (validity.error) {
      validity.error.name = 'invalidInput';
      next(validity.error);
    } else {
      return;
    }
  }
});

// DELETE STUFF
router.delete('/:id', async (req, res, next) => {
  validateID();

  try {
    await checkDataOwner();
    let dashboard = await deleteDashboard();
    res.send(`${dashboard[0].name} has been deleted`);
  } catch (err) {
    err.name = 'database';
    next(err);
  }

  function deleteDashboard() {
    return knex('dashboards')
      .returning('name')
      .where({ id: req.params.id, tid: req.user.tid })
      .delete();
  }

  function checkDataOwner() {
    return knex('dashboards')
      .select('id')
      .where({ id: req.params.id, tid: req.user.tid })
      .then(data => {
        if (data.length > 0) {
          return;
        } else {
          err = new Error();
          err.name = 'no-matches';
          return next(err);
        }
      });
  }

  function validateID() {
    const validity = validate.databaseID(req.params.id);
    if (validity.error) {
      validity.error.name = 'invalidInput';
      next(validity.error);
    } else {
      return;
    }
  }
});

// router.delete('/column', async (req, res, next) => {
//   validateID();

//   try {
//     await checkDataOwner();
//     let column = await deleteColumn();
//     res.send(column[0].name, ' has been deleted');
//   } catch (err) {
//     err.name = 'database';
//     next(err);
//   }

//   function deleteColumn() {
//     return knex('columns')
//       .returning('name')
//       .where({ id: req.body.id, tid: req.user.tid })
//       .delete();
//   }

//   function checkDataOwner() {
//     knex('columns')
//       .select('id')
//       .where({ id: req.body.id, tid: req.user.tid })
//       .then(data => {
//         if (data.length > 0) {
//           return;
//         } else {
//           err = new Error();
//           err.name = 'not-your-data';
//           return next(err);
//         }
//       });
//   }

//   function validateID() {
//     const validity = validate.databaseID(req.body.id);
//     if (validity.error) {
//       validity.error.name = 'invalidInput';
//       next(validity.error);
//     } else {
//       return;
//     }
//   }
// });

// router.post('/positions', async (req, res, next) => {
//   validateAll();
//   try {
//     let dashboards = await repositionDashboards();
//     res.send(dashboards, ' dashboards repositioned');
//   } catch (err) {
//     err.name = 'database';
//     next(err);
//   }

//   function repositionDashboards() {
//     let dashboardsUpdated = 0;
//     for (let position of req.body.positions) {
//       knex('dashboards')
//         .update({
//           position: position.position,
//           modified: knex.raw('SYSDATETIME()')
//         })
//         .where({ tid: req.user.tid, id: position.id })
//         .then(data => {
//           dashboardsUpdated++;
//         });
//     }
//     return dashboardsUpdated;
//   }

//   function validateAll() {
//     for (let position of req.body.positions) {
//       const validity = validate.positions(position);
//       if (validity.error) {
//         validity.error.name = 'invalidInput';
//         next(validity.error);
//         break;
//       } else {
//         for (let id of req.body.ids) {
//           const validity = validate.databaseID(id);
//           if (validity.error) {
//             validity.error.name = 'invalidInput';
//             next(validity.error);
//             break;
//           } else {
//             return;
//           }
//         }
//       }
//     }
//   }
// });

router.post('/default', async (req, res, next) => {
  validateID();

  try {
    collection = await checkDataOwner();
    await setDefaultDashboard();
    res.send(`${collection} updated`);
  } catch (err) {
    err.name = 'database';
    next(err);
  }

  function setDefaultDashboard() {
    return knex('collections')
      .update({
        default_dashboard: req.body.dashboard
      })
      .where({ tid: req.user.tid, id: req.body.collection });
  }

  function checkDataOwner() {
    return knex('collections')
      .select('name')
      .where({ id: req.body.collection, tid: req.user.tid })
      .then(data => {
        if (data.length > 0) {
          return data[0].name;
        } else {
          err = new Error();
          err.name = 'not-your-data';
          return next(err);
        }
      });
  }

  function validateID() {
    const validity = validate.databaseID(req.body.dashboard);
    if (validity.error) {
      validity.error.name = 'invalidInput';
      next(validity.error);
    } else {
      return;
    }
  }
});

module.exports = router;
