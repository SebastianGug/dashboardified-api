const express = require('express');
const router = express.Router();
const mssql = require('mssql');
const appconfig = require('config');
const crypto = require('../utils/encryption');
const Joi = require('joi');
const validate = require('../validators/tenant-validation');
const adminTenant = appconfig.get('config.adminTenant');
// KNEX configuration
let knex = require('knex')({
  client: 'mssql',
  connection: {
    host: appconfig.get('dashboardified.address'),
    user: appconfig.get('dashboardified.username'),
    password: appconfig.get('dashboardified.password'),
    database: 'dashboardified',
    encrypt: true
  }
});

router.get('/billing-status', (req, res) => {
  knex
    .select()
    .from('billing_status')
    .catch(err => {
      console.log(err.message);
      res.status(500).send('Database error when reading billing statuses.');
    })
    .then(data => {
      res.send(data);
    });
});

router.get('/operational-status', (req, res) => {
  knex
    .select()
    .from('operational_status')
    .catch(err => {
      console.log(err.message);
      res.status(500).send('Database error when reading operational statuses.');
    })
    .then(data => {
      res.send(data);
    });
});

router.get('/all', (req, res) => {
  if (req.user.tid === adminTenant) {
    knex
      .select(
        'tenants.*',
        'billing_status.name AS billing_status',
        'operational_status.name AS operational_status'
      )
      .from('tenants')
      .leftJoin('billing_status', 'tenants.billing_key', 'billing_status.id')
      .leftJoin(
        'operational_status',
        'tenants.operational_key',
        'operational_status.id'
      )
      .catch(err => {
        console.log(err.message);
        res.status(500).send('Database error when reading tenants.');
      })
      .then(data => {
        res.send(data);
      });
  } else {
    res.status(401).send('You are not authorised to read all tenants');
  }
});

router.get('/:id', (req, res) => {
  if (req.user.tid === adminTenant) {
    const validity = validate.singleTenant(req.params.id);

    if (validity.error) {
      res.status(400).send(validity.error.details[0].message);
    } else {
      knex
        .select(
          'tenants.*',
          'billing_status.name AS billing_status',
          'operational_status.name AS operational_status'
        )
        .from('tenants')
        .where({ 'tenants.tid': req.params.tid })
        .leftJoin('billing_status', 'tenants.billing_key', 'billing_status.id')
        .leftJoin(
          'operational_status',
          'tenants.operational_key',
          'operational_status.id'
        )
        .catch(err => {
          console.log(err.message);
          res.status(500).send('Database error when reading tenant.');
        })
        .then(data => {
          if (data[0]) {
            res.send(data);
          } else {
            res.status(400).send('Tenant could not be found.');
          }
        });
    }
  } else {
    res.status(401).send('You are not authorised to get tenant.');
  }
});

router.post('/new', (req, res) => {
  const validity = validate.newTenant(
    req.body.name,
    req.body.description,
    req.body.db_address,
    req.body.db_user,
    req.body.db_password,
    req.user.tid,
    req.body.billing_key,
    req.body.operational_key
  );

  if (validity.error) {
    res.status(400).send(validity.error.details[0].message);
  } else {
    knex('tenants')
      .where({ tid: req.user.tid })
      .select('tid')
      .catch(err => {
        console.log(err.message);
        res
          .status(500)
          .send('Database error when checking if tenant is unique.');
      })
      .then(data => {
        if (data[0]) {
          res.status(400).send('Tenant already exists in the database.');
        } else {
          knex('tenants')
            .insert({
              name: req.body.name,
              description: req.body.description,
              joined: knex.raw('SYSDATETIME()'),
              db_address: req.body.db_address,
              db_user: req.body.db_user,
              db_password: crypto.encrypt(req.body.db_password),
              tid: req.user.tid,
              billing_key: req.body.billing_key,
              operational_key: req.body.operational_key
            })
            .catch(err => {
              console.log(err.message);
              res
                .status(500)
                .send('Database error when trying to add tenant to database.');
            })
            .then(data => {
              updateCatalog();

              knex
                .select(
                  'tenants.*',
                  'billing_status.name AS billing_status',
                  'operational_status.name AS operational_status'
                )
                .from('tenants')
                .where({ 'tenants.tid': req.body.tid })
                .leftJoin(
                  'billing_status',
                  'tenants.billing_key',
                  'billing_status.id'
                )
                .leftJoin(
                  'operational_status',
                  'tenants.operational_key',
                  'operational_status.id'
                )
                .catch(err => {
                  console.log(err.message);
                  res
                    .status(500)
                    .send('Database error when reading added tenant.');
                })
                .then(data => {
                  res.send(data);
                });
            });
        }
      });
  }
});

router.put('/update', (req, res) => {
  const validity = validate.existingTenant(
    req.body.id,
    req.body.name,
    req.body.description,
    req.body.db_address,
    req.body.db_user,
    req.body.db_password,
    req.user.tid,
    req.body.billing_key,
    req.body.operational_key
  );

  if (validity.error) {
    res.status(400).send(validity.error.details[0].message);
  } else {
    knex('tenants')
      .where({ tid: req.user.tid })
      .select('tid')
      .catch(err => {
        console.log(err.message);
        res.status(500).send('Database error when trying to find tenant.');
      })
      .then(data => {
        if (data[0]) {
          knex('tenants')
            .where('id', req.body.id)
            .update({
              name: req.body.name,
              description: req.body.description,
              joined: knex.raw('SYSDATETIME()'),
              db_address: req.body.db_address,
              db_user: req.body.db_user,
              db_password: crypto.encrypt(req.body.db_password),
              tid: req.user.tid,
              billing_key: req.body.billing_key,
              operational_key: req.body.operational_key
            })
            .catch(err => {
              console.log(err.message);
              res.status(500).send('Database error when updating tenant data.');
            })
            .then(data => {
              knex
                .select(
                  'tenants.*',
                  'billing_status.name AS billing_status',
                  'operational_status.name AS operational_status'
                )
                .from('tenants')
                .where({ 'tenants.tid': req.user.tid })
                .leftJoin(
                  'billing_status',
                  'tenants.billing_key',
                  'billing_status.id'
                )
                .leftJoin(
                  'operational_status',
                  'tenants.operational_key',
                  'operational_status.id'
                )
                .catch(err => {
                  console.log(err.message);
                  res.status(500).send('Database error when reading tenant.');
                })
                .then(data => {
                  res.send(data);
                });
            });
        } else {
          res.status(400).send('Cannot find tenant in the database.');
        }
      });
  }
});

router.delete('/delete/:id', (req, res) => {
  if (req.user.tid === adminTenant) {
    const validity = validate.singleTenant(req.params.id);

    if (validity.error) {
      res.status(400).send(validity.error.details[0].message);
    } else {
      knex('tenants')
        .where({ id: req.params.id })
        .select('id')
        .catch(err => {
          console.log(err.message);
          res.status(500).send('Database error when reading tenant.');
        })
        .then(data => {
          if (data[0]) {
            knex('tenants')
              .where({ id: req.params.id })
              .delete()
              .then(data => {
                res.send('Tenant deleted successfully.');
              });
          } else {
            res.status(400).send('Tenant could not be found.');
          }
        });
    }
  } else {
    res.status(401).send('You are not authorized to delete tenants.');
  }
});

router.post('/report', (req, res) => {
  const validity = validate.reportIssue(req.body.subject, req.body.message);

  if (validity.error) {
    res.status(400).send(validity.error.details[0].message);
  } else {
    knex('reports')
      .insert({
        subject: req.body.subject,
        message: req.body.message,
        email: req.user.upn,
        tid: req.user.tid,
        date: knex.raw('SYSDATETIME()')
      })
      .catch(err => {
        console.log(err.message);
        res.status(500).send('Database error when trying to log message.');
      })
      .then(data => {
        res.send('Report successfully registered.');
      });
  }
});

router.get('/test', (req, res) => {});

async function updateCatalog() {
  try {
    knex
      .select('tenants.tid')
      .from('tenants')
      .leftJoin('billing_status', 'tenants.billing_key', 'billing_status.id')
      .leftJoin(
        'operational_status',
        'tenants.operational_key',
        'operational_status.id'
      )
      .catch(err => {
        console.log(err.message);
        res.status(500).send('Database error when reading tenants.');
      })
      .then(data => {
        console.log('CATALOG LOADED');
        global.tenantCatalog = data;
      });
  } catch (err) {
    console.log('ERROR UPDATING TENANTS', err);
  }
}

module.exports = router;
