const express = require('express');
const router = express.Router();
const appconfig = require('config');
const validate = require('../validators/dashboardified-validation');
const rbac = require('../middleware/rbac.middleware');
const knex = require('knex')({
  client: 'mssql',
  connection: {
    host: appconfig.get('dashboardified.address'),
    user: appconfig.get('dashboardified.username'),
    password: appconfig.get('dashboardified.password'),
    database: 'dashboardified',
    encrypt: true
  }
});

router.use(rbac);

router.post('', async (req, res, next) => {
  validateCollection();

  try {
    let collection = await insertCollection();
    res.send(collection);
  } catch (err) {
    err.name = 'database';
    next(err);
  }

  // FUNCTIONS BEING USED
  function validateCollection() {
    const validity = validate.newCollection(
      req.body.name,
      req.body.description
    );

    if (validity.error) {
      validity.error.name = 'invalidInput';
      next(validity.error);
    } else {
      return;
    }
  }

  function insertCollection() {
    return knex('collections')
      .returning('*')
      .insert({
        name: req.body.name,
        description: req.body.description,
        modified: knex.raw('SYSDATETIME()'),
        tid: req.user.tid,
        position: 51
      });
  }
});

router.post('/default', async (req, res, next) => {
  validateID();

  try {
    collection = await checkDataOwner();
    await setDefaultCollection();
    res.send(`${collection} is now the default collection`);
  } catch (err) {
    err.name = 'database';
    next(err);
  }

  function setDefaultCollection() {
    return knex('tenants')
      .update({
        default_collection: req.body.collection
      })
      .where({ tid: req.user.tid });
  }

  async function checkDataOwner() {
    return knex('collections')
      .select('name')
      .where({ id: req.body.collection, tid: req.user.tid })
      .then(data => {
        if (data.length > 0) {
          return data[0].name;
        } else {
          err = new Error();
          err.name = 'no-matches';
          return next(err);
        }
      });
  }

  function validateID() {
    const validity = validate.databaseID(req.body.collection);
    if (validity.error) {
      validity.error.name = 'invalidInput';
      next(validity.error);
    } else {
      return;
    }
  }
});

router.put('', async (req, res, next) => {
  validateCollection();
  validateID();

  try {
    await checkDataOwner();
    let collection = await updateCollection();
    res.send(collection);
  } catch (err) {
    err.name = 'database';
    next(err);
  }

  // QUERIES BEING USED

  // FUNCTIONS BEING USED
  function validateCollection() {
    const validity = validate.newCollection(
      req.body.name,
      req.body.description,
      req.body.position
    );

    if (validity.error) {
      validity.error.name = 'invalidInput';
      next(validity.error);
    } else {
      return;
    }
  }

  function validateID() {
    const validity = validate.databaseID(req.body.id);
    if (validity.error) {
      validity.error.name = 'invalidInput';
      next(validity.error);
    } else {
      return;
    }
  }

  function checkDataOwner() {
    knex('collections')
      .select('id')
      .where({ id: req.body.id, tid: req.user.tid })
      .then(data => {
        if (data.length > 0) {
          return;
        } else {
          err = new Error();
          err.name = 'no-matches';
          return next(err);
        }
      });
  }

  function updateCollection() {
    return knex('collections')
      .returning('*')
      .update({
        name: req.body.name,
        description: req.body.description,
        modified: knex.raw('SYSDATETIME()')
      })
      .where({ tid: req.user.tid, id: req.body.id });
  }
});

router.delete('/:id', async (req, res, next) => {
  validateID();

  try {
    await checkDataOwner();
    let collection = await deleteCollection();
    console.log('THE COLLECTION IS THIS: ', collection);
    res.send(`${collection[0]} has been deleted`);
  } catch (err) {
    err.name = 'database';
    next(err);
  }

  function deleteCollection() {
    return knex('collections')
      .returning('name')
      .where({ id: req.params.id, tid: req.user.tid })
      .delete();
  }

  async function checkDataOwner() {
    return knex('collections')
      .select('id')
      .where({ id: req.params.id, tid: req.user.tid })

      .then(data => {
        console.log(data, 'THIS IS THE DATA');
        if (data.length > 0) {
          return;
        } else {
          console.log(data);
          err = new Error();
          err.name = 'no-matches';
          return next(err);
        }
      });
  }

  function validateID() {
    const validity = validate.databaseID(req.params.id);
    if (validity.error) {
      validity.error.name = 'invalidInput';
      next(validity.error);
    } else {
      return;
    }
  }
});

router.post('/positions', async (req, res, next) => {
  validateAll();
  try {
    let collections = await repositionCollections();
    res.send(collections, ' collections repositioned');
  } catch (err) {
    err.name = 'database';
    next(err);
  }

  function repositionCollections() {
    let columnsUpdated = 0;
    for (let position of req.body.positions) {
      knex('collections')
        .update({
          position: position.position,
          modified: knex.raw('SYSDATETIME()')
        })
        .where({ tid: req.user.tid, id: position.id })
        .then(data => {
          columnsUpdated++;
        });
    }
    return columnsUpdated;
  }

  function validateAll() {
    for (let position of req.body.positions) {
      const validity = validate.positions(position);
      if (validity.error) {
        validity.error.name = 'invalidInput';
        next(validity.error);
        break;
      } else {
        for (let id of req.body.ids) {
          const validity = validate.databaseID(id);
          if (validity.error) {
            validity.error.name = 'invalidInput';
            next(validity.error);
            break;
          } else {
            return;
          }
        }
      }
    }
  }
});

module.exports = router;
