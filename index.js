// DEPENDENCIES
const express = require('express');
const appconfig = require('config');
const socket = require('socket.io');
const helmet = require('helmet');
const app = express();
const bodyParser = require('body-parser');
const BearerStrategy = require('passport-azure-ad').BearerStrategy;
const bunyan = require('bunyan');
const cors = require('cors');

// CUSTOM MIDDLEWARE
const tenantChecker = require('./middleware/tenantChecker.middleware');
const errorHandler = require('./middleware/errorHandler.middleware');

// PASSPORT AUTHENTICATION
const passport = require('passport');
const tenants = require('./routes/tenants');

// ACTUAL ROUTES
const dashboardified = require('./routes/readActions');
const collections = require('./routes/collections');
const dashboards = require('./routes/dashboards');
const signup = require('./routes/signup');
const playground = require('./routes/playground');

// KNEX configuration
const knex = require('knex')({
  client: 'mssql',
  connection: {
    host: appconfig.get('dashboardified.address'),
    user: appconfig.get('dashboardified.username'),
    password: appconfig.get('dashboardified.password'),
    database: 'dashboardified',
    encrypt: true
  }
});

// STARTING SERVER
const port = process.env.PORT || 8080;
let server = app.listen(port, () => {
  console.log(`App Live running on port: ${port}`);
});

// LOADING CATALOG

knex
  .select('tenants.tid')
  .from('tenants')
  .leftJoin('billing_status', 'tenants.billing_key', 'billing_status.id')
  .leftJoin(
    'operational_status',
    'tenants.operational_key',
    'operational_status.id'
  )
  .catch(err => {
    console.log(err.message);
    res.status(500).send('Database error when reading tenants.');
  })
  .then(data => {
    console.log('CATALOG LOADED');
    global.tenantCatalog = data;
  });
// Socket Server
let io = socket(server);
console.log(`NODE_ENV: ${process.env.NODE_ENV}`);
console.log(appconfig.get('creds.identityMetadata'));

//BEARER TOKEN AUTHENTICATION
let options = {
  identityMetadata: appconfig.get('creds.identityMetadata'),
  clientID: appconfig.get('creds.clientID'),
  passReqToCallback: false,
  loggingLevel: 'info',
  allowMultiAudiencesInToken: true,
  validateIssuer: false
};

let bearerStrategy = new BearerStrategy(options, function(token, done) {
  log.info(token, 'was the token retreived');
  if (!token.oid) return done(new Error('oid is not found in token'));
  else {
    owner = token.oid;
    return done(null, token);
  }
});

let log = bunyan.createLogger({
  name: 'Bunyan Logger AD thing test'
});

let corsOptions = {
  origin: 'http://localhost:4200'
};

// SOME SETTINGS
app.use(cors());
app.use(express.json());
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());
passport.use(bearerStrategy);

app.use('/playground', playground);

// ROUTES
app.use(
  '/read',
  passport.authenticate('oauth-bearer', { session: false }),
  dashboardified
);

app.use(
  '/tenants',
  passport.authenticate('oauth-bearer', { session: false }),
  tenantChecker,
  tenants
);

app.use(
  '/signup',
  passport.authenticate('oauth-bearer', { session: false }),
  signup
);
app.use(
  '/collections',
  passport.authenticate('oauth-bearer', { session: false }),
  tenantChecker,
  collections
);
app.use(
  '/dashboards',
  passport.authenticate('oauth-bearer', { session: false }),
  tenantChecker,
  dashboards
);

// ERROR HANDLER AND AFTER-TYPE MIDDLEWARE
app.use(errorHandler);

io.on('connection', function(socket) {
  console.log('Made Socket Connection', socket.id);

  socket.on('disconnect', function() {
    console.log('Socket disconnected', socket.id);
  });

  socket.on('message', message => {
    console.log('Message Received: ' + message);
    io.emit('message', { type: 'new-message', text: message });
  });
});
