const Joi = require('joi');

module.exports = {
  newSignup: function validateSignup(
    email,
    firstName,
    lastName,
    accessRequested
  ) {
    const schema = {
      email: Joi.string()
        .email()
        .required(),
      firstName: Joi.string()
        .min(2)
        .max(50)
        .required(),
      lastName: Joi.string()
        .min(2)
        .max(50)
        .required(),
      accessRequested: Joi.boolean().required()
    };

    return Joi.validate(
      {
        email: email,
        firstName: firstName,
        lastName: lastName,
        accessRequested: accessRequested
      },
      schema
    );
  }
};
