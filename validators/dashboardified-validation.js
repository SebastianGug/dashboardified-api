const Joi = require('joi');

module.exports = {
  newCollection: function validateCollection(name, description, position) {
    const schema = {
      name: Joi.string()
        .min(2)
        .max(50)
        .required(),
      description: Joi.string()
        .min(2)
        .max(200)
        .required()
    };

    return Joi.validate(
      {
        name: name,
        description: description
      },
      schema
    );
  },

  databaseID: function validateID(id) {
    const schema = {
      id: Joi.number()
        .min(1)
        .max(2147483647)
        .required()
    };

    return Joi.validate(
      {
        id: id
      },
      schema
    );
  },

  newDashboard: function validateDashboard(
    name,
    description,
    collection,
    site,
    list
  ) {
    const schema = {
      name: Joi.string()
        .min(2)
        .max(50)
        .required(),
      description: Joi.string()
        .min(2)
        .max(200)
        .required(),
      collection: Joi.number()
        .min(1)
        .max(2147483647)
        .required(),
      site: Joi.string()
        .min(2)
        .max(300)
        .required(),
      list: Joi.string()
        .min(2)
        .max(300)
        .required()
    };

    return Joi.validate(
      {
        name: name,
        description: description,
        collection: collection,
        site,
        list
      },
      schema
    );
  },

  updateDashboard: function validateDashboard(name, description, site, list) {
    const schema = {
      name: Joi.string()
        .min(2)
        .max(50)
        .required(),
      description: Joi.string()
        .min(2)
        .max(200)
        .required(),
      site: Joi.string()
        .min(2)
        .max(50)
        .required(),
      list: Joi.string()
        .min(2)
        .max(50)
        .required()
    };

    return Joi.validate(
      {
        name: name,
        description: description,
        site: site,
        list: list
      },
      schema
    );
  },

  newColumn: function validateColumn(column) {
    const schema = {
      displayName: Joi.string()
        .min(2)
        .max(50)
        .required(),
      name: Joi.string()
        .min(1)
        .max(100)
        .required(),
      isDate: Joi.boolean().allow(null),
      isLocation: Joi.boolean().allow(null),
      isLatitude: Joi.boolean().allow(null),
      isLongitude: Joi.boolean().allow(null),
      isLegend: Joi.boolean().allow(null)
    };

    return Joi.validate(
      {
        displayName: column.displayName,
        name: column.name,
        isDate: column.isDate,
        isLocation: column.isLocation,
        isLatitude: column.isLatitude,
        isLongitude: column.isLongitude,
        isLegend: column.isLegend
      },
      schema
    );
  },

  positions: function validatePositions(position) {
    const schema = {
      position: Joi.number()
        .min(1)
        .max(50)
        .required()
    };

    return Joi.validate(
      {
        position: position
      },
      schema
    );
  },

  name: function validateName(name) {
    const schema = {
      name: Joi.string()
        .min(2)
        .max(50)
        .required()
    };

    return Joi.validate(
      {
        name: name
      },
      schema
    );
  },

  existingColumns: function validateExistingColumns(column) {
    const schema = {
      id: Joi.number()
        .min(1)
        .max(2147483647)
        .required(),
      displayName: Joi.string()
        .min(2)
        .max(50)
        .required(),
      name: Joi.string()
        .min(1)
        .max(100)
        .required(),
      isDate: Joi.boolean().allow(null),
      isLocation: Joi.boolean().allow(null),
      isLatitude: Joi.boolean().allow(null),
      isLongitude: Joi.boolean().allow(null),
      isLegend: Joi.boolean().allow(null),
      position: Joi.number()
        .min(0)
        .max(50)
        .required()
    };

    return Joi.validate(
      {
        id: column.id,
        displayName: column.displayName,
        name: column.name,
        isDate: column.isDate,
        isLocation: column.isLocation,
        isLatitude: column.isLatitude,
        isLongitude: column.isLongitude,
        isLegend: column.isLegend,
        position: column.position
      },
      schema
    );
  }
};
