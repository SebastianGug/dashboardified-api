const Joi = require('joi');

module.exports = {
  newTenant: function validateTenant(
    name,
    description,
    db_address,
    db_user,
    db_password,
    tid,
    billing_key,
    operational_key
  ) {
    const schema = {
      name: Joi.string()
        .min(3)
        .max(2147483647)
        .required(),
      description: Joi.string()
        .min(5)
        .max(50)
        .required(),
      db_address: Joi.string()
        .min(5)
        .max(50)
        .required(),
      db_user: Joi.string()
        .min(10)
        .max(50)
        .required(),
      db_password: Joi.string()
        .min(10)
        .max(50)
        .required(),
      tid: Joi.string()
        .min(34)
        .max(40)
        .required(),
      billing_key: Joi.number()
        .min(1)
        .max(2147483647)
        .required(),
      operational_key: Joi.number()
        .min(1)
        .max(2147483647)
        .required()
    };

    return Joi.validate(
      {
        name: name,
        description: description,
        db_address: db_address,
        db_user: db_user,
        db_password: db_password,
        tid: tid,
        billing_key: billing_key,
        operational_key: operational_key
      },
      schema
    );
  },
  existingTenant: function validateTenant(
    id,
    name,
    description,
    db_address,
    db_user,
    db_password,
    tid,
    billing_key,
    operational_key
  ) {
    const schema = {
      id: Joi.number()
        .min(1)
        .max(2147483647)
        .required(),
      name: Joi.string()
        .min(3)
        .max(50)
        .required(),
      description: Joi.string()
        .min(5)
        .max(50)
        .required(),
      db_address: Joi.string()
        .min(5)
        .max(50)
        .required(),
      db_user: Joi.string()
        .min(10)
        .max(50)
        .required(),
      db_password: Joi.string()
        .min(10)
        .max(50)
        .required(),
      tid: Joi.string()
        .min(34)
        .max(40)
        .required(),
      billing_key: Joi.number()
        .min(1)
        .max(2147483647)
        .required(),
      operational_key: Joi.number()
        .min(1)
        .max(2147483647)
        .required()
    };

    return Joi.validate(
      {
        id: id,
        name: name,
        description: description,
        db_address: db_address,
        db_user: db_user,
        db_password: db_password,
        tid: tid,
        billing_key: billing_key,
        operational_key: operational_key
      },
      schema
    );
  },
  singleTenant: function validateTenant(id) {
    const schema = {
      id: Joi.number()
        .min(1)
        .max(2147483647)
        .required()
    };

    return Joi.validate(
      {
        id: id
      },
      schema
    );
  },
  reportIssue: function validateReportMessage(subject, message) {
    const schema = {
      subject: Joi.string()
        .min(2)
        .max(50)
        .required(),
      message: Joi.string()
        .min(5)
        .max(500)
        .required()
    };

    return Joi.validate(
      {
        subject: subject,
        message: message
      },
      schema
    );
  }
};
