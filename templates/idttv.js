// Outside dependencies
const express = require('express');
const router = express.Router();
const sql = require('mssql');
const appconfig = require('config');
const Joi = require('joi');
// Configuring connection to database
const config = {
  user: appconfig.get('idtTV.username'),
  password: appconfig.get('idtTV.password'),
  server: appconfig.get('idtTV.address'),
  database: 'A14PWA',
  options: {
    encrypt: true
  }
};

// Roles relevant to idtTV
const admin = 'idtTVAdmin';
const editor = 'idtTVEditor';
const guest = 'idtTVGuest';

// Routes
router.get('/videos/:range', (req, res) => {
  startRange = req.params.range - 10;
  // SQL Queries
  guestQuery = `SELECT [dbo].[idtTV_videos].*, [dbo].[idtTV_categories].[name] AS [category]
    FROM [dbo].[idtTV_videos] WHERE [a14_only] = 0 LEFT JOIN [dbo].[idtTV_categories]
    ON [dbo].[idtTV_videos].[categories] = [dbo].[idtTV_categories].[id] ORDER BY [title] OFFSET ${startRange} ROWS FETCH NEXT ${
    req.params.range
  } ROWS ONLY `;
  standardQuery = `SELECT [dbo].[idtTV_videos].*, [dbo].[idtTV_categories].[name] AS [category]
    FROM [dbo].[idtTV_videos] LEFT JOIN [dbo].[idtTV_categories]
    ON [dbo].[idtTV_videos].[categories] = [dbo].[idtTV_categories].[id] ORDER BY [title] OFFSET ${startRange} ROWS FETCH NEXT ${
    req.params.range
  } ROWS ONLY`;

  // The function that starts the flow based on roles.
  if (req.user.roles.some(role => role === guest)) {
    queryDatabaseGet(guestQuery, res);
  } else {
    queryDatabaseGet(standardQuery, res);
  }
});

router.get('/videos/category/:category/:range', (req, res) => {
  startRange = req.params.range - 10;

  // SQL Queries
  guestQuery = `SELECT [dbo].[idtTV_videos].*, [dbo].[idtTV_categories].[name] AS [category]
                    FROM [dbo].[idtTV_videos] LEFT JOIN [dbo].[idtTV_categories]
                    ON [dbo].[idtTV_videos].[categories] = [dbo].[idtTV_categories].[id] WHERE [dbo].[idtTV_videos].[a14_only] = 1 AND [dbo].[idtTV_videos].[categories] LIKE '%${
                      req.query.category
                    }%' ORDER BY [title] OFFSET ${startRange} ROWS FETCH NEXT ${
    req.params.range
  } ROWS ONLY`;
  standardQuery = `SELECT [dbo].[idtTV_videos].*, [dbo].[idtTV_categories].[name] AS [category]
                    FROM [dbo].[idtTV_videos] LEFT JOIN [dbo].[idtTV_categories]
                    ON [dbo].[idtTV_videos].[categories] = [dbo].[idtTV_categories].[id] WHERE [dbo].[idtTV_videos].[categories] LIKE '%${
                      req.params.category
                    }%' ORDER BY [title] OFFSET ${startRange} ROWS FETCH NEXT ${
    req.params.range
  } ROWS ONLY `;

  // The function that starts the flow based on roles.

  if (req.user.roles.some(role => role === guest)) {
    queryDatabaseGet(guestQuery, res);
  } else {
    queryDatabaseGet(standardQuery, res);
  }
});

router.get('/categories', (req, res) => {
  standardQuery = `SELECT [id]
    ,[name]
    ,[description]
    ,[created_time]
    ,[created_by] from [dbo].[idtTV_categories]`;

  queryDatabaseGet(standardQuery, res);
});

router.get('/video/:id', (req, res) => {
  guestQuery = `SELECT [dbo].[idtTV_videos].*, [dbo].[idtTV_categories].[name] AS [category]
    FROM [dbo].[idtTV_videos] LEFT JOIN [dbo].[idtTV_categories]
    ON [dbo].[idtTV_videos].[categories] = [dbo].[idtTV_categories].[id] WHERE [a14_only] = 0 AND [dbo].[idtTV_videos].[id] =${
      req.params.id
    }`;
  standardQuery = `SELECT [dbo].[idtTV_videos].*, [dbo].[idtTV_categories].[name] AS [category]
    FROM [dbo].[idtTV_videos] LEFT JOIN [dbo].[idtTV_categories]
    ON [dbo].[idtTV_videos].[categories] = [dbo].[idtTV_categories].[id]  WHERE [dbo].[idtTV_videos].[id] =${
      req.params.id
    }`;

  if (req.user.roles.some(role => role === guest)) {
    queryDatabaseGet(guestQuery, res);
  } else {
    queryDatabaseGet(standardQuery, res);
  }
});

router.post('/video', (req, res) => {
  let privacySetting;

  if (req.body.visibleToGuests == true) {
    privacySetting = 1;
  } else {
    privacySetting = 0;
  }

  let videoLink = escapeCharacters(req.body.link);

  uniqueQuery = `SELECT [link] from [dbo].[idtTV_videos] WHERE [link] = '${videoLink}'`;

  writeQuery = `INSERT INTO [dbo].[idtTV_videos] ([title],[description],[creator],[created_time],[link],[source],[a14_only],[categories]) 
    VALUES 
    ('${req.body.title}', 
    '${req.body.description}',
    '${req.user.name}',
    SYSDATETIME(),
    '${videoLink}',
    'Vimeo',
    '${privacySetting}',
    '${req.body.category}')`;

  getQuery = `SELECT [dbo].[idtTV_videos].*, [dbo].[idtTV_categories].[name] AS [category]
    FROM [dbo].[idtTV_videos] LEFT JOIN [dbo].[idtTV_categories]
    ON [dbo].[idtTV_videos].[categories] = [dbo].[idtTV_categories].[id]  WHERE [dbo].[idtTV_videos].[link]= '${videoLink}'`;

  const schema = Joi.object().keys({
    title: Joi.string()
      .min(3)
      .max(50)
      .required(),
    description: Joi.string()
      .min(5)
      .max(200)
      .required(),
    link: Joi.string()
      .max(100)
      .required(),
    visibleToGuests: Joi.boolean().required(),
    category: Joi.number().required()
  });

  if (req.user.roles.some(role => role === admin || role === editor)) {
    const result = Joi.validate(
      {
        title: req.body.title,
        description: req.body.description,
        link: videoLink,
        visibleToGuests: req.body.visibleToGuests,
        category: req.body.category
      },
      schema
    );
    if (result.error) {
      res.status(400).send(result.error.details[0].message);
    } else {
      queryDatabaseUnique(uniqueQuery, writeQuery, getQuery, res);
    }
  } else {
    res.status(520).send(`${req.user.name} is not authorized to Add Videos.`);
  }
});

router.put('/video/:id', (req, res) => {
  let privacySetting;
  let videoTitle = escapeCharacters(req.body.title);
  let videoDescription = escapeCharacters(req.body.description);

  if (req.body.visibleToGuests == true) {
    privacySetting = 1;
  } else {
    privacySetting = 0;
  }

  existsQuery = `SELECT [id] from [dbo].[idtTV_videos] WHERE [id] = '${
    req.params.id
  }'`;

  writeQuery = `
    UPDATE [dbo].[idtTV_videos] 
    SET [title] = '${videoTitle}', 
    [description] = '${videoDescription}', 
    [creator] = '${req.user.name}', 
    [created_time] = SYSDATETIME(), 
    [link] = '${req.body.link}',
    [source]= 'Vimeo',
    [a14_only]= ${privacySetting},
    [categories]= ${req.body.category}
    WHERE [id] = ${req.params.id}`;

  getQuery = `SELECT [dbo].[idtTV_videos].*, [dbo].[idtTV_categories].[name] AS [category]
    FROM [dbo].[idtTV_videos] LEFT JOIN [dbo].[idtTV_categories]
    ON [dbo].[idtTV_videos].[categories] = [dbo].[idtTV_categories].[id]  WHERE [dbo].[idtTV_videos].[id] =${
      req.params.id
    }`;

  const schema = Joi.object().keys({
    id: Joi.number()
      .min(1)
      .required(),
    title: Joi.string()
      .min(3)
      .max(50)
      .required(),
    description: Joi.string()
      .min(5)
      .max(200)
      .required(),
    link: Joi.string()
      .max(100)
      .required(),
    visibleToGuests: Joi.boolean().required(),
    category: Joi.number().required()
  });

  if (req.user.roles.some(role => role === admin || role === editor)) {
    const result = Joi.validate(
      {
        id: req.params.id,
        title: req.body.title,
        description: req.body.description,
        link: req.body.link,
        visibleToGuests: req.body.visibleToGuests,
        category: req.body.category
      },
      schema
    );
    if (result.error) {
      res.status(400).send(result.error.details[0].message);
    } else {
      queryDatabaseExists(existsQuery, writeQuery, getQuery, res);
    }
  } else {
    res
      .status(520)
      .send(`${req.user.name} is not authorized to update Videos.`);
  }
});

router.delete('/video/:id', (req, res) => {
  existsQuery = `SELECT [id] from [dbo].[idtTV_videos] WHERE [id] = '${
    req.params.id
  }'`;

  writeQuery = `
    DELETE FROM [dbo].[idtTV_videos] 
    WHERE [id] = ${req.params.id}`;

  getQuery = `SELECT [id] from [dbo].[idtTV_videos] WHERE [id] = '${
    req.params.id
  }'`;

  const schema = Joi.object().keys({
    id: Joi.number()
      .integer()
      .min(1)
      .required()
  });

  if (req.user.roles.some(role => role === admin || role === editor)) {
    const result = Joi.validate({ id: req.params.id }, schema);
    if (result.error) {
      res.status(400).send(result.error.details[0].message);
    } else {
      queryDatabaseExists(existsQuery, writeQuery, getQuery, res);
    }
  } else {
    (res.statusMessage = req.user.name),
      ' is not authorized to delete videos on idtTV';
    res.status(401);
  }
});

router.post('/admin/category', (req, res) => {
  let categoryName = escapeCharacters(req.body.name);
  let categoryDescription = escapeCharacters(req.body.description);

  uniqueQuery = `SELECT [name] from [dbo].[idtTV_categories] WHERE [name] = '${
    req.body.name
  }'`;

  writeQuery = `INSERT INTO [dbo].[idtTV_categories] ([name],[description],[created_time],[created_by]) 
    VALUES 
    ('${categoryName}', 
    '${categoryDescription}',
    SYSDATETIME(),
    '${req.user.name}')`;

  getQuery = `SELECT [id]
    ,[name]
    ,[description]
    ,[created_time]
    ,[created_by] from [dbo].[idtTV_categories]`;

  const schema = Joi.object().keys({
    name: Joi.string()
      .min(3)
      .max(50)
      .required(),
    description: Joi.string()
      .min(5)
      .max(200)
      .required()
  });

  if (req.user.roles.some(role => role === admin || role === editor)) {
    const result = Joi.validate(
      { name: categoryName, description: categoryDescription },
      schema
    );
    if (result.error) {
      res.status(400).send(result.error.details[0].message);
    } else {
      queryDatabaseUnique(uniqueQuery, writeQuery, getQuery, res);
    }
  } else {
    res
      .status(520)
      .send(`${req.user.name} is not authorized to update categories.`);
  }
});

router.put('/admin/category/:id', (req, res) => {
  let categoryName = escapeCharacters(req.body.name);
  let categoryDescription = escapeCharacters(req.body.description);

  existsQuery = `SELECT [id] from [dbo].[idtTV_categories] WHERE [id] = ${
    req.params.id
  }`;

  writeQuery = `UPDATE [dbo].[idtTV_categories]
    SET [name] = '${categoryName}',
    [description] = '${categoryDescription}',
    [created_time] = SYSDATETIME(),
    [created_by] = '${req.user.name}'
    WHERE [id] = ${req.params.id}`;

  getQuery = `SELECT [id]
    ,[name]
    ,[description]
    ,[created_time]
    ,[created_by] from [dbo].[idtTV_categories]`;

  const schema = Joi.object().keys({
    id: Joi.number()
      .integer()
      .min(1)
      .required()
  });

  if (req.user.roles.some(role => role === admin || role === editor)) {
    const result = Joi.validate({ id: req.params.id }, schema);
    if (result.error) {
      res.status(400).send(result.error.details[0].message);
    } else {
      queryDatabaseExists(existsQuery, writeQuery, getQuery, res);
    }
  } else {
    (res.statusMessage = req.user.name),
      ' is not authorized to update categories on idtTV';
    res.status(401);
  }
});

router.delete('/admin/category/:id', (req, res) => {
  isUsedQuery = `SELECT * from [dbo].[idtTV_videos] WHERE [categories] = ${
    req.params.id
  }`;

  writeQuery = `
    DELETE FROM [dbo].[idtTV_categories] 
    WHERE [id] = ${req.params.id}`;

  getQuery = `SELECT [id]
    ,[name]
    ,[description]
    ,[created_time]
    ,[created_by] from [dbo].[idtTV_categories]`;

  const schema = Joi.object().keys({
    id: Joi.number()
      .integer()
      .min(1)
      .required()
  });

  if (req.user.roles.some(role => role === admin || role === editor)) {
    const result = Joi.validate({ id: req.params.id }, schema);
    if (result.error) {
      res.status(400).send(result.error.details[0].message);
    } else {
      queryDatabaseIsUsed(isUsedQuery, writeQuery, getQuery, res);
    }
  } else {
    (res.statusMessage = req.user.name),
      ' is not authorized to delete categories on idtTV';
    res.status(401);
  }
});

///////////// MAIN QUERING FUNCTION //////////////////////

async function queryDatabaseGet(getQuery, res) {
  try {
    const cp = new sql.ConnectionPool(config);
    await cp.connect();
    let result = await cp.request().query(getQuery);
    res.send(result.recordset);
  } catch (err) {
    res.status(520).send(`Database error when getting items: ${err}`);
  }
}

async function queryDatabaseExists(existsQuery, modifyQuery, getQuery, res) {
  try {
    const cp = new sql.ConnectionPool(config);
    await cp.connect();
    let result = await cp.request().query(existsQuery);
    if (result.rowsAffected[0] > 0) {
      queryDatabaseWrite(modifyQuery, getQuery, res);
    } else {
      res.status(400).send(`Cannot find item in database.`);
    }
  } catch (err) {
    res.status(520).send(`Database error when checking if exists: ${err}`);
  }
}

async function queryDatabaseUnique(uniqueQuery, writeQuery, getQuery, res) {
  try {
    const cp = new sql.ConnectionPool(config);
    await cp.connect();
    let result = await cp.request().query(uniqueQuery);
    if (result.rowsAffected[0] == 0) {
      queryDatabaseWrite(writeQuery, getQuery, res);
    } else {
      res
        .status(400)
        .send(`Item already exists in database ${result.rowsAffected[0]}`);
    }
  } catch (err) {
    res.status(520).send(`Database error when checking if unique: ${err}`);
  }
}

async function queryDatabaseIsUsed(isUsedQuery, modifyQuery, getQuery, res) {
  try {
    const cp = new sql.ConnectionPool(config);
    await cp.connect();
    let result = await cp.request().query(isUsedQuery);
    if (result.rowsAffected[0] == 0) {
      queryDatabaseWrite(modifyQuery, getQuery, res);
    } else {
      res.status(400).send(`Category in use by videos, cannot delete.`);
    }
  } catch (err) {
    res.status(520).send(`Database error when checking if used: ${err}`);
  }
}

async function queryDatabaseWrite(writeQuery, getQuery, res) {
  try {
    const cp = new sql.ConnectionPool(config);
    await cp.connect();
    let result = await cp.request().query(writeQuery);
    if (result.rowsAffected[0] > 0) {
      queryDatabaseGet(getQuery, res);
    } else {
      res.status(400).send(`Error Writing to Database`, result.rowsAffected[0]);
    }
  } catch (err) {
    res.status(520).send(`Database error when writing: ${err}`);
  }
}

function escapeCharacters(val) {
  val = val.replace(/[\0\n\r\b\t\\'"\x1a]/g, function(s) {
    switch (s) {
      case '\0':
        return '\\0';
      case '\n':
        return '\\n';
      case '\r':
        return '\\r';
      case '\b':
        return '\\b';
      case '\t':
        return '\\t';
      case '\x1a':
        return '\\Z';
      case "'":
        return "''";
      case '"':
        return '""';
      default:
        return '\\' + s;
    }
  });

  return val;
}

module.exports = router;
