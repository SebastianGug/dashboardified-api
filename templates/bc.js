const express = require("express");
const router = express.Router();
const sql = require("mssql");
const appconfig = require("config");

const config = {
    user: appconfig.get("readalldbconnection.username"),
    password: appconfig.get("readalldbconnection.password"),
    server: appconfig.get("idtTV.address"),
    database: "BC_kol_warehouse",
    options: {
        encrypt: true
    }
};

// GET REQ WITH PARAMETERS AND SOME SQL INJECTION

// RETURNS ALL UNIQUE SECTIONS
router.get("/initialising", (req, res) => {

    sendInitialArray();

    async function sendInitialArray() {
        try {
            let queryResult = await getQuery();
            res.send(queryResult);
        } catch (error) {

              res.status(520).send(`Database error: ${error}`);
        }
    }

    async function getQuery() {
        try {
            const cp = new sql.ConnectionPool(config);
            await cp.connect();
            let result = await cp.request().query(`
                SELECT DISTINCT [md:0/413349530_Level 0 – Section] 
                FROM [BC_kol_warehouse].[dbo].[BCQueryResult]
                WHERE [md:0/413349530_Level 0 – Section] IS NOT NULL 
                AND [md:0/413349530_Level 0 – Section] != '\\0Section 1\\0Section 2\\0' 
                AND [md:0/413349530_Level 0 – Section] != '\\0Section 3A\\0Section 3B\\0'
                AND [md:0/413349530_Level 0 – Section] != '' ORDER BY [md:0/413349530_Level 0 – Section] 
                `);
            cp.close();
            return result.recordset;
        } catch (error) {
        res.status(520).send(`Database error: ${error}`);
        }
    }
});

// RETURNS ALL UNIQUE SERIES
router.get("/lvl1", (req, res) => {
    sendCustomArray();

    async function sendCustomArray() {
        try {
            const queryParam = req.query.lvl1param;
            let queryResult = await getCustomQuery(queryParam);
            res.send(queryResult);
        } catch (error) {
        res.status(520).send(`Database error: ${error}`);
        }
    }

    async function getCustomQuery(param) {
        try {
            const cp = new sql.ConnectionPool(config);
            await cp.connect();
            let result = await cp.request().query(`
                SELECT DISTINCT [md:0/413349530_Level 0 – Section], 
                [md:0/413349601_Level 1 – Series Header]
                FROM [BC_kol_warehouse].[dbo].[BCQueryResult] 
                WHERE [md:0/413349530_Level 0 – Section] IS NOT NULL
                AND  [md:0/413349601_Level 1 – Series Header] IS NOT NULL
                AND [md:0/413349530_Level 0 – Section] LIKE '%${param}%' ORDER BY [md:0/413349601_Level 1 – Series Header]`);
            cp.close();
            return result.recordset;
        } catch (error) {
        res.status(520).send(`Database error: ${error}`);
        }
    }
});

// RETURNS ALL UNIQUE SUBHEADERS
router.get("/lvl2", (req, res) => {
    sendCustomArray();

    async function sendCustomArray() {
        try {
            const querylvl1 = req.query.lvl1param;
            const querylvl2 = req.query.lvl2param;
            let queryResult = await getCustomQuery(querylvl1, querylvl2);
            res.send(queryResult);
        } catch (error) {
        res.status(520).send(`Database error: ${error}`);
        }
    }

    async function getCustomQuery(param1, param2) {
        try {
            const cp = new sql.ConnectionPool(config);
            await cp.connect();
            let result = await cp.request().query(`
                SELECT DISTINCT [md:0/413349530_Level 0 – Section], 
                [md:0/413349601_Level 1 – Series Header], 
                [md:0/413349649_Level 2 – Series Sub Header] 
                FROM [BC_kol_warehouse].[dbo].[BCQueryResult] 
                WHERE [md:0/413349649_Level 2 – Series Sub Header]  IS NOT NULL
                AND [md:0/413349530_Level 0 – Section] LIKE '%${param1}%'
                AND [md:0/413349601_Level 1 – Series Header] LIKE '%${param2}%' ORDER BY  [md:0/413349649_Level 2 – Series Sub Header]
                `);
            cp.close();
            return result.recordset;
        } catch (error) {
            res.status(520).send(`Database error: ${error}`);  }      
    }
});

// RETURNS ALL UNIQUE LOCATIONS
router.get("/lvl3", (req, res) => {
    sendCustomArray();

    async function sendCustomArray() {
        try {
            const querylvl1 = req.query.lvl1param;
            const querylvl2 = req.query.lvl2param;
            const querylvl3 = req.query.lvl3param;
            let queryResult = await getCustomQuery(querylvl1, querylvl2, querylvl3);
            res.send(queryResult);
        } catch (error) {
            res.status(520).send(`Database error: ${error}`); } 
    }

    async function getCustomQuery(param1, param2, param3) {
        try {
            const cp = new sql.ConnectionPool(config);
            await cp.connect();
            let result = await cp.request().query(`
                SELECT DISTINCT[md:0/413349530_Level 0 – Section], 
                [md:0/413349601_Level 1 – Series Header], 
                [md:0/413349649_Level 2 – Series Sub Header], 
                [md:0/413349651_Level 3 – Location]
                FROM [BC_kol_warehouse].[dbo].[BCQueryResult] 
                WHERE  [md:0/413349651_Level 3 – Location] IS NOT NULL
                AND [md:0/413349530_Level 0 – Section] LIKE '%${param1}%'
                AND [md:0/413349601_Level 1 – Series Header] LIKE '%${param2}%' 
                AND [md:0/413349649_Level 2 – Series Sub Header] LIKE '%${param3}%' ORDER BY [md:0/413349651_Level 3 – Location] 
                `);
            cp.close();
            return result.recordset;
        } catch (error) {
        res.status(520).send(`Database error: ${error}`);    }        
    }
});


// RETURNS ALL DOCUMENTS BASED ON LOCATION + SUBHEADER + SERIES + SECTION
router.get("/lvl5docs", (req, res) => {
    sendCustomArray();

    async function sendCustomArray() {
        try {
            const querylvl1 = req.query.lvl1param;
            const querylvl2 = req.query.lvl2param;
            const querylvl3 = req.query.lvl3param;
            const querylvl4 = req.query.lvl4param;
            let queryResult = await getCustomQuery(
                querylvl1,
                querylvl2,
                querylvl3,
                querylvl4
            );
            res.send(queryResult);
        } catch (error) {
            res.status(520).send(`Database error: ${error}`);
        }   
    }

    async function getCustomQuery(param1, param2, param3, param4) {
        try {
            const cp = new sql.ConnectionPool(config);
            await cp.connect();
            let result = await cp.request().query(`
                SELECT DISTINCT 
                [md:0/320497435_12 Document Date],
                [md:0/325125307_Transmittal No.],
                [md:0/320497442_10 Revision],
                [name],
                [descr],
                [version_label],
                [artifact_type_name],
                [id]
                FROM [BC_kol_warehouse].[dbo].[BCQueryResult] 
                WHERE [md:0/413349530_Level 0 – Section] LIKE '%${param1}%' 
                AND [md:0/413349601_Level 1 – Series Header] LIKE '%${param2}%' 
                AND [md:0/413349649_Level 2 – Series Sub Header] LIKE '%${param3}%' 
                AND [md:0/413349651_Level 3 – Location] LIKE '%${param4}%' ORDER BY [name]
                `);
            cp.close();
            return result.recordset;
        } catch (error) {
            res.status(520).send(`Database error: ${error}`); } 
    }
});

// RETURNS ALL DOCUMENTS BASED ON SERIES + SECTION
router.get("/lvl2docs", (req, res) => {
    sendCustomArray();

    async function sendCustomArray() {
        try {
            const querylvl1 = req.query.lvl1param;
            const querylvl2 = req.query.lvl2param;
            const querylvl3 = req.query.lvl3param;
            const querylvl4 = req.query.lvl4param;
            let queryResult = await getCustomQuery(
                querylvl1,
                querylvl2,
                querylvl3,
                querylvl4
            );
            res.send(queryResult);
        } catch (error) {
        res.status(520).send(`Database error: ${error}`);
        }
    }

    async function getCustomQuery(param1, param2, param3, param4) {
        try {
            const cp = new sql.ConnectionPool(config);
            await cp.connect();
            let result = await cp.request().query(`
                SELECT DISTINCT 
                [md:0/320497435_12 Document Date],
                [md:0/325125307_Transmittal No.],
                [md:0/320497442_10 Revision],
                [name],
                [descr],
                [version_label],
                [artifact_type_name],
                [id]
                FROM [BC_kol_warehouse].[dbo].[BCQueryResult] 
                WHERE [md:0/413349530_Level 0 – Section] LIKE '%${param1}%' 
                AND [md:0/413349601_Level 1 – Series Header] LIKE '%${param2}%' 
                ORDER BY [name]
                `);
            cp.close();

            return result.recordset;
        } catch (error) {
        res.status(520).send(`Database error: ${error}`);
        }
    }
});

// RETURNS ALL DOCUMENTS BASED ON SUBHEADER + SERIES + SECTION
router.get("/lvl3docs", (req, res) => {
    sendCustomArray();

    async function sendCustomArray() {
        try {
            const querylvl1 = req.query.lvl1param;
            const querylvl2 = req.query.lvl2param;
            const querylvl3 = req.query.lvl3param;
            const querylvl4 = req.query.lvl4param;
            let queryResult = await getCustomQuery(
                querylvl1,
                querylvl2,
                querylvl3,
                querylvl4
            );
            res.send(queryResult);
        } catch (error) {
        res.status(520).send(`Database error: ${error}`);
        }
    }

    async function getCustomQuery(param1, param2, param3, param4) {
        try {
            const cp = new sql.ConnectionPool(config);
            await cp.connect();
            let result = await cp.request().query(`
                SELECT DISTINCT 
                [md:0/320497435_12 Document Date],
                [md:0/325125307_Transmittal No.],
                [md:0/320497442_10 Revision],
                [name],
                [descr],
                [version_label],
                [artifact_type_name],
                [id]
                FROM [BC_kol_warehouse].[dbo].[BCQueryResult] 
                WHERE [md:0/413349530_Level 0 – Section] LIKE '%${param1}%' 
                AND [md:0/413349601_Level 1 – Series Header] LIKE '%${param2}%' 
                AND [md:0/413349649_Level 2 – Series Sub Header] LIKE '%${param3}%' 
                ORDER BY [name]
                `);
            cp.close();
            return result.recordset;
        } catch (error) {
        res.status(520).send(`Database error: ${error}`);
        }
    }
});

module.exports = router;
